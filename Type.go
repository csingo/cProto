package cProto

type ProtoService struct {
	ServiceName string
	ServiceRpc  []*ProtoService_Rpc
}

type ProtoService_Rpc struct {
	RpcName     string
	RpcRequest  string
	RpcResponse string
}

type ProtoField struct {
	MessageName string
	FieldName   string
	SourceType  string
	TargetType  string
}

type ProtoEnum struct {
	EnumName   string
	EnumFields []*ProtoEnum_Field
}

type ProtoEnum_Field struct {
	EnumFieldName  string
	EnumFieldValue string
	EnumFieldDesc  string
}

type ProtoType struct {
	MessageName string
	Field       string
	OldType     string
	NewType     string
}
